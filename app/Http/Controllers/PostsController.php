<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\WpPost as Posts;

class PostsController extends Controller {

    public function home() {
        return view('home')->with('posts', Posts::getSlideshowPosts());
    }

    public function content() {
       return view('reportagens')->with('posts', Posts::getPosts());
    }

    public function videos() {
       return view('videos');
    }

    public function mapas() {
       return view('mapas');
    }

    public function docs() {
       return view('docs');
    }

    public function infog() {
       return view('infog');
    }

    public function projeto() {
       return view('projeto');
    }

    public function quemsomos() {
       return view('quem-somos');
    }

    public function parceiro() {
       return view('parceiro');
    }

    public function contato() {
       return view('contato');
    }

}
