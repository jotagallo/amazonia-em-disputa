<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Corcel\Post;
use Illuminate\Support\Facades\DB;

class WpPost extends Post
{

    public static function getAmazoniaIDs() {
        $posts = DB::select("SELECT p.ID FROM wp_qm1dcq_posts AS p "
          . "INNER JOIN wp_qm1dcq_term_relationships as tr ON tr.object_id = p.ID "
          . "WHERE tr.term_taxonomy_id = '2191' OR tr.term_taxonomy_id = '2010' GROUP BY p.ID, p.post_date ORDER by p.post_date DESC ;");
        return $posts;
    }

    public static function getPosts() {
        $posts = [];
        foreach (self::getAmazoniaIDs() as $id) {
            $posts[] = self::find($id->ID);
        }
        return $posts;
    }

    public static function getSlideshowPosts() {
        $posts = self::getPosts();
        $home = self::homePosts();
        foreach ($posts as $key => $post) {
            if (!array_key_exists($post->ID, $home)) {
                unset($posts[$key]);
            } else {
                $post->post_title = $home[$post->ID];
            }
        }
        return $posts;
    }

    public function getImagemPrincipal() {
        $img = $this->find($this->meta->_thumbnail_id);
        $aws = $img->meta->amazonS3_info;
        if (!is_array($aws)) {
            $str = mb_convert_encoding($aws, 'Windows-1252', 'UTF-8');
            $needle = 'wp-content';
            $end = '.jpg';
            $pos = stripos($str, $needle);
            $pos2 = stripos($str, $end);
            if (!$pos2) {
                $pos2 = stripos($str, '.png');
            }
            $url = substr($str, $pos, $pos2  - $pos + strlen($end));
            return 'http://s3-sa-east-1.amazonaws.com/apublica-files-main/'.$url;
        }
        return is_array($aws) ? 'http://s3-' . $aws['region'] . '.amazonaws.com/' . $aws['bucket'] . '/' . $aws['key'] : '';
    }

    public static function homePosts() {
        return [
            26651 => 'PÚBLICA REVELA CRIME',

            26642 => 'MORTES SOB ENCOMENDA',

            26432 => 'A ESPERA QUE SANGRA',

            25868 => 'AMEAÇA AOS XAVANTES',

            25896 => 'O MARTÍRIO DO BISPO',

            25402 => 'GUERRA PELA BAUXITA',

            25384 => 'ACORDO NA ÁRVORE',

            24966 => 'FLORESTA NA BOLSA',

            24944 => 'CAR, CRIME E GRILAGEM',

            24912 => 'AS REVELAÇÕES DO CAR',

            24019 => 'FUNAI PEDE SOCORRO',

            22708 => 'ASSENTAMENTOS QUE DESMATAM',

            23425 => 'A TENSÃO INDÍGENA COM TEMER',

            23368 => 'CONGRESSO “SEM PERFIL”',

            24113 => 'MINERAÇÃO BATE À PORTA',

            24069 => 'GUARDIÕES DA TERRA',

            22825 => 'DESMANDOS DO INCRA',

            22743 => '“QUADRO CATASTRÓFICO”'
        ];
    }

}
