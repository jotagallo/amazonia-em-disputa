<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PostsController@home');
Route::get('/reportagens', 'PostsController@content');
Route::get('/videos', 'PostsController@videos');
Route::get('/mapas', 'PostsController@mapas');
Route::get('/base-de-dados', 'PostsController@docs');
Route::get('/infograficos', 'PostsController@infog');
Route::get('/o-projeto', 'PostsController@projeto');
Route::get('/quem-somos', 'PostsController@quemsomos');
Route::get('/parceiro', 'PostsController@parceiro');
Route::get('/contato', 'PostsController@contato');


