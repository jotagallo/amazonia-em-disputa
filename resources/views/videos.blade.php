@extends('layouts.app')

@section('content')
<h2>Vídeos</h2>

<h3/>Nos vídeos a seguir, a Pública explica a questão indígena, a grilagem de terras, a pistolagem e o conflito agrário na Amazônia Legal</h3>

<p class="lead section-lead"><strong>O silêncio do Divino Pai Eterno</strong>
<br />Uma história de pistolagem e luta pela terra no sul e sudeste do Pará</p>
<iframe src="//player.vimeo.com/video/187926221" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<p class="lead section-lead"><strong>A questão indígena em quatro minutos</strong>
<br />No vídeo a seguir, contamos a história contemporânea da questão indígena brasileira</p>
<iframe src="//player.vimeo.com/video/170930305" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<p class="lead section-lead"><strong>A grilagem de terras e o Novo Código Florestal em 3 minutos</strong>
<br />No vídeo a seguir, contamos a história da grilagem e como ela influencia as atuais políticas ambientais</p>
<iframe src="//player.vimeo.com/video/176506903" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<p class="lead section-lead"><strong>A resistência Quilombola em Oriximiná</strong>
<br />A Pública mostra a briga entre uma mineradora de bauxita e comunidades quilombolas</p>
<iframe src="//player.vimeo.com/video/179495868" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<!-- O silêncio do Divino Pai Eterno
No sudoeste do Pará a pistolagem e o conflito agrário numa área pública em disputa
[LINK] -->

@endsection