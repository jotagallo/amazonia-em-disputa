@extends('layouts.app')

@section('content')
<h2>Reportagens</h2>
    <section id="posts">
      @foreach ($posts as $post)
           <article class="white-panel"> <img src="{{ $post->getImagemPrincipal() }}" alt="">
            <h4><a href="{{ $post->guid }}" target="_blank">{{ mb_convert_encoding($post->post_title, 'Windows-1252', 'UTF-8') }}</a></h4>
            <span class="autores"></span>
            <p>{{ utf8_decode($post->post_excerpt) }}</p>
          </article>
      @endforeach
    </section>
@endsection