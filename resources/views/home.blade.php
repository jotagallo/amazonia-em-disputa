@extends('layouts.app')

@section('content')

  <section class="responsive slider">
  @foreach ($posts as $key => $post)
    <div>
      <a href="{{ $post->guid }}" target="_blank"><img src="{{ $post->getImagemPrincipal() }}" />
        <h4 class="slider-title">{{ $post->post_title }}</h4>
      </a>
    </div>
  @endforeach
  </section>

@endsection