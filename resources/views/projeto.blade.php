@extends('layouts.app')

@section('content')
<h2>O Projeto</h2>

<p class="lead section-lead">“Amazônia em Disputa” é um projeto especial da Agência Pública com o objetivo de investigar a atuação das agências governamentais em meio a disputa predatória por terra na maior floresta tropical do planeta.</p>

<p class="lead section-lead">Durante três meses, levantamos e cruzamos dados sobre o Incra, a Funai, o ICMBio e o Serviço Florestal Brasileiro buscando avaliar o controle do Estado sobre o território e a execução das políticas públicas de prevenção ao desmatamento e aos conflitos por terra.</p>

<p class="lead section-lead">Munidos desses dados, partimos para mais três meses de reportagens em campo nos estados do Pará e Mato Grosso – os mais desmatados da Amazônia – com o objetivo de apurar o resultado dessas políticas na vida dos que ali vivem.</p>

<p class="lead section-lead">Do destino dessas comunidades – extremamente vulneráveis - depende o futuro da floresta, como constatamos ao longo dessa investigação.</p>

<p class="lead section-lead">Aqui você tem acesso ao resultado completo desse trabalho por meio de vídeos, fotos, mapas, infográficos e reportagens.</p>

@endsection