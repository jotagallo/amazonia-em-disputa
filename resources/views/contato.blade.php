@extends('layouts.app')

@section('content')
<h2>Contato</h2>

<p class="lead section-lead">
Telefone:<br />
55 11 3661.3887<br />
Email: contato@apublica.org<br />
Endereço:<br />
Rua Vitorino Carmilo, 453 – casa 2<br />
Barra Funda – São Paulo, SP – Brasil – 01153-000<br />
<a href="http://apublica.org" target="_blank">http://apublica.org</a>
</p>
@endsection