@extends('layouts.app')

@section('content')
<h2>Infográficos</h2>

<h3>A Pública fez três infográficos que explicam a Mineração em Terras Indígenas. </h3>

<div class="infog-img">
  <img src="/img/AF-AgPublica_Infografico-bloco-01_Mineracao-Terra-Indigena.png" />
  <img src="/img/AF-AgPublica_Infografico-bloco-02_ProcessosMinerarios.png" />
  <img src="/img/AF-AgPublica_Infografico-bloco-03_Pressoes.png" />
  <p class="lead section-lead">
    No GIF a seguir, como a terra indígena mantém a floresta em pé
  </p>
  <img src="/img/AF-AgPublica_Infografico-bloco-04_Desmatamento.gif" />
</div>

<p class="lead section-lead">
  As principais descobertas das falhas e inconsistência do Cadastro Ambiental Rural (CAR) no Pará foram consolidadas nesse infográfico.
</p>

<h4>Cadastro Ambiental Rural no Pará</h4>
<div class="tabs-left et_sliderfx_fade et_sliderauto_false et_sliderauto_speed_5000 et_slidertype_left_tabs clearfix" style="position: relative;"><ul class="et_shortcodes_mobile_nav"><li><a href="#" class="et_sc_nav_next">Next<span></span></a></li><li><a href="#" class="et_sc_nav_prev">Previous<span></span></a></li></ul>
  <div class="et_left_tabs_bg"></div>
  <ul class="et-tabs-control">
    <li class="active"><a href="#">
        O cadastro
      </a></li>
    <li><a href="#">
        A pesquisa
      </a></li>
    <li><a href="#">
        Sobreposições
      </a></li>
    <li><a href="#">
        Áreas protegidas
      </a></li>
    <li><a href="#">
        Top TIs afetadas
      </a></li>
    <li><a href="#">
        Top UCs afetadas
      </a></li>
    <li><a href="#">
        Glossário
      </a></li>
    <li><a href="#">
        Compartilhe
      </a></li>
  </ul>
  <div class="et-tabs-content" style="overflow: hidden; position: relative;">
    <div class="et-tabs-content-main-wrap">
      <div class="et-tabs-content-wrapper">
        <div class="et_slidecontent et_shortcode_slide_active" style="display: block;">
          <a href="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002118/tela-1.jpg" data-rel="lightbox-0" title=""><img class="aligncenter size-full wp-image-24891" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002118/tela-1.jpg" alt="tela-1" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002118/tela-1.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002118/tela-1-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002118/tela-1-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px" data-lazy-loaded="true" style="display: block;"><noscript>&lt;img class="aligncenter size-full wp-image-24891" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002118/tela-1.jpg" alt="tela-1" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002118/tela-1.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002118/tela-1-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002118/tela-1-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px"/&gt;</noscript></a>
        </div>
        <div class="et_slidecontent">
          <a href="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002146/tela-2.jpg" data-rel="lightbox-1" title=""><img class="aligncenter wp-image-24893 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002146/tela-2.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002146/tela-2.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002146/tela-2-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002146/tela-2-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px" data-lazy-loaded="true" style="display: block;"><noscript>&lt;img class="aligncenter wp-image-24893 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002146/tela-2.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002146/tela-2.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002146/tela-2-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002146/tela-2-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px"/&gt;</noscript></a>
        </div>
        <div class="et_slidecontent">
          <a href="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002156/tela-3.jpg" data-rel="lightbox-2" title=""><img class="aligncenter wp-image-24894 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002156/tela-3.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002156/tela-3.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002156/tela-3-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002156/tela-3-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px" data-lazy-loaded="true" style="display: block;"><noscript>&lt;img class="aligncenter wp-image-24894 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002156/tela-3.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002156/tela-3.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002156/tela-3-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002156/tela-3-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px"/&gt;</noscript></a>
        </div>
        <div class="et_slidecontent">
          <a href="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002206/tela-4.jpg" data-rel="lightbox-3" title=""><img class="aligncenter wp-image-24895 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002206/tela-4.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002206/tela-4.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002206/tela-4-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002206/tela-4-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px" data-lazy-loaded="true" style="display: block;"><noscript>&lt;img class="aligncenter wp-image-24895 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002206/tela-4.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002206/tela-4.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002206/tela-4-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002206/tela-4-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px"/&gt;</noscript></a>
        </div>
        <div class="et_slidecontent">
          <a href="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002215/tela-5.jpg" data-rel="lightbox-4" title=""><img class="aligncenter wp-image-24896 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002215/tela-5.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002215/tela-5.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002215/tela-5-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002215/tela-5-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px" data-lazy-loaded="true" style="display: block;"><noscript>&lt;img class="aligncenter wp-image-24896 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002215/tela-5.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002215/tela-5.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002215/tela-5-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002215/tela-5-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px"/&gt;</noscript></a>
        </div>
        <div class="et_slidecontent">
          <a href="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002225/tela-6.jpg" data-rel="lightbox-5" title=""><img class="aligncenter wp-image-24897 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002225/tela-6.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002225/tela-6.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002225/tela-6-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002225/tela-6-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px" data-lazy-loaded="true" style="display: block;"><noscript>&lt;img class="aligncenter wp-image-24897 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002225/tela-6.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002225/tela-6.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002225/tela-6-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2016/07/31002225/tela-6-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px"/&gt;</noscript></a>
        </div>
        <div class="et_slidecontent">
          <a href="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2013/07/01113713/tela-10.jpg" data-rel="lightbox-6" title=""><img class="aligncenter wp-image-24929 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2013/07/01113713/tela-10.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2013/07/01113713/tela-10.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2013/07/01113713/tela-10-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2013/07/01113713/tela-10-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px" data-lazy-loaded="true" style="display: block;"><noscript>&lt;img class="aligncenter wp-image-24929 size-full" src="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2013/07/01113713/tela-10.jpg" width="800" height="800" srcset="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2013/07/01113713/tela-10.jpg 800w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2013/07/01113713/tela-10-134x134.jpg 134w, http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2013/07/01113713/tela-10-600x600.jpg 600w" sizes="(max-width: 800px) 100vw, 800px"/&gt;</noscript></a>
        </div>
        <div class="et_slidecontent">
          Para embedar esse infográfico, <a target="_blank" href="http://s3-sa-east-1.amazonaws.com/apublica-files-main/wp-content/uploads/2013/07/01113954/codigo.txt"><strong>copie esse código</strong></a> e cole no editor de texto de seu site<p></p>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection