@extends('layouts.app')

@section('content')
<h2>Base de Dados</h2>

<p class="lead section-lead">
  A Pública organizou e padronizou a base de dados utilizada durante as apurações das reportagens e as disponibiliza para outros pesquisadores.
</p>
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          <span class="glyphicon glyphicon-plus"></span>
          Cadastro Ambiental Rural
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
      <div class="panel-body">
        <h5><a href="/files/car/CAR_AreasProtegidas_Definitivo.xlsx"><span class="green glyphicon glyphicon-file">CAR_AreasProtegidas_Definitivo.xlsx</span></a></h5>
        <h5><a href="/files/car/CAR_Sobrepoe_Quilombo.xlsx"><span class="green glyphicon glyphicon-file">CAR_Sobrepoe_Quilombo.xlsx</span></a></h5>
        <h5><a href="/files/car/CAR_Sobrepoe_TI.xlsx"><span class="green glyphicon glyphicon-file">CAR_Sobrepoe_TI.xlsx</span></a></h5>
        <h5><a href="/files/car/CAR_Sobrepoe_UCSus.xlsx"><span class="green glyphicon glyphicon-file">CAR_Sobrepoe_UCSus.xlsx</span></a></h5>
        <h5><a href="/files/car/CAR_SobreposicaoImovel_SICAR.xlsx"><span class="green glyphicon glyphicon-file">CAR_SobreposicaoImovel_SICAR.xlsx</span></a></h5>
        <h5><a href="/files/car/CAR_Demonstrativo.csv"><span class="blue glyphicon glyphicon-file">CAR_Demonstrativo.csv</span></a></h5>
        <h5><a href="/files/car/CAR_RazaoSocial.csv"><span class="blue glyphicon glyphicon-file">CAR_RazaoSocial.csv</span></a></h5>
        <h5><a href="/files/car/CAR_RestricaoImovel.csv"><span class="blue glyphicon glyphicon-file">CAR_RestricaoImovel.csv</span></a></h5>
        <h5><a href="/files/car/CAR_SobreposicoesTotal.csv"><span class="blue glyphicon glyphicon-file">CAR_SobreposicoesTotal.csv</span></a></h5>

      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          <span class="glyphicon glyphicon-plus"></span>
          Comunidades Tradicionais
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <h5><a href="/files/comunidades-tradicionais/CT_Mineracao_TI.xlsx"><span class="green glyphicon glyphicon-file">CT_Mineracao_TI.xlsx</span></a></h5>
        <h5><a href="/files/comunidades-tradicionais/CT_OrcamentoFunai_Final.xlsx"><span class="green glyphicon glyphicon-file">CT_OrcamentoFunai_Final.xlsx</span></a></h5>
        <h5><a href="/files/comunidades-tradicionais/CT_Quilombolas.xlsx"><span class="green glyphicon glyphicon-file">CT_Quilombolas.xlsx</span></a></h5>
        <h5><a href="/files/comunidades-tradicionais/CT_TerrasIndigenas.xlsx"><span class="green glyphicon glyphicon-file">CT_TerrasIndigenas.xlsx</span></a></h5>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          <span class="glyphicon glyphicon-plus"></span>
          Meio Ambiente
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        <h5><a href="/files/meio-ambiente/MA_Autuacoes.xls"><span class="green glyphicon glyphicon-file">MA_Autuacoes.xls</span></a></h5>
        <h5><a href="/files/meio-ambiente/MA_Embargos.xlsx"><span class="green glyphicon glyphicon-file">MA_Embargos.xlsx</span></a></h5>
        <h5><a href="/files/meio-ambiente/MA_LicenciamentoAmbiental.xlsx"><span class="green glyphicon glyphicon-file">MA_LicenciamentoAmbiental.xlsx</span></a></h5>
        <h5><a href="/files/meio-ambiente/MA_UCs.xlsx"><span class="green glyphicon glyphicon-file">MA_UCs.xlsx</span></a></h5>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
          <span class="glyphicon glyphicon-plus"></span>
          Terras
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
      <div class="panel-body">
        <h5><a href="/files/terras/TE_Assentamentos.xlsx"><span class="green glyphicon glyphicon-file">TE_Assentamentos.xlsx</span></a></h5>
        <h5><a href="/files/terras/TE_HomologacoesAssentamentos.xlsx"><span class="green glyphicon glyphicon-file">TE_HomologacoesAssentamentos.xlsx</span></a></h5>
        <h5><a href="/files/terras/TE_Sigef_2014.xls"><span class="green glyphicon glyphicon-file">TE_Sigef_2014.xls</span></a></h5>
        <h5><a href="/files/terras/TE_SNCI_privado.xlsx"><span class="green glyphicon glyphicon-file">TE_SNCI_privado.xlsx</span></a></h5>
        <h5><a href="/files/terras/TE_SNCI_publico.xlsx"><span class="green glyphicon glyphicon-file">TE_SNCI_publico.xlsx</span></a></h5>
      </div>
    </div>
  </div>
</div>

@endsection