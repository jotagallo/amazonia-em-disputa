@extends('layouts.app')

@section('content')
<h2>Quem Somos</h2>

<p class="lead section-lead">
<strong>Coordenação e edição:</strong> Marina Amaral e Thiago Domenici<br />
<strong>Redação:</strong> Ciro Barros (texto) e Iuri Barcelos (dados e vídeo)<br />
<strong>Reportagens no Pará</strong> Ana Mendes (foto e vídeo), Ciro Barros (texto), José Cícero da Silva (foto e vídeo) e Tomás Chiaverini (texto)<br />
<strong>Reportagens no Mato Grosso:</strong> Lucas Ferraz (texto) e Rai Reis (foto e vídeo)<br />
<strong>Arte dos textos:</strong> Caco Bressane (infografia, ilustração e mapas) e Bruno Fonseca (infografia)<br />
<strong>Arte dos vídeos:</strong> Caetano Patta e Julio Falas <br />
<strong>Divulgação:</strong> Marina Dias e Nyle Ferrari<br />
<strong>Revisão:</strong> Lilian do Amaral Vieira e Ricardo Jensen<br />
<strong>Desenvolvimento Web:</strong> João Otávio Gallo<br />
</p>
@endsection
