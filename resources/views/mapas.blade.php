@extends('layouts.app')

@section('content')
<h2>Mapas</h2>

<h3>Três mapas mostram as regiões percorridas em campo pela reportagem da Pública.</h3>

<a class="mapas" href="http://apublica.org/2016/09/no-mato-grosso-os-novos-problemas-de-uma-velha-disputa/" target="_blank"><img class="mapa" width="100%" src="/img/AF_APublica_Mapa-Maraiwatsede.jpg" /></a>
<h5>Mato Grosso | Como o impeachment abriu um novo capítulo para um dos conflitos por terra mais antigos da Amazônia, na área indígena mais desmatada do país</h5>
<a href="/img/AF_APublica_Mapa-Maraiwatsede.jpg" target="_blank">Ver em Alta Resolução</a>

<a class="mapas" href="http://apublica.org/2016/08/a-guerra-secreta-pela-bauxita/" target="_blank"><img class="mapa" width="100%" src="/img/AF_Base_MapaBauxita_v3.png" /></a>
<h5>Pará | A briga de quilombolas pela posse de terras esbarra em interesses da Mineração Rio do Norte</h5>
<a href="/img/AF_Base_MapaBauxita_v3.png" target="_blank">Ver em Alta Resolução</a>

<a class="mapas" href="http://apublica.org/2016/10/a-espera-que-sangra-o-divino-pai-eterno/" target="_blank"><img class="mapa" width="100%" src="/img/AF_DivinoPaiEterno_alta.jpg" /></a>
<h5>Pará | Agricultores exigem criação de assentamento em fazenda localizada em terra pública; conflito violento expõe a negligência do Estado</h5>
<a href="/img/AF_DivinoPaiEterno_alta.jpg" target="_blank">Ver em Alta Resolução</a>

@endsection