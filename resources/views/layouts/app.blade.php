<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Especial Amazônia em Disputa - Agência Pública</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <link href="css/slick.css" rel="stylesheet">
    <link href="css/slick-theme.css" rel="stylesheet">
    <link href="css/shortcodes.css" rel="stylesheet">
    <link href="css/global.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Lora" rel="stylesheet">

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/pinterest-grid.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/global.js"></script>
    <script src="js/slick.js"></script>
    <script src="js/shortcodes.js"></script>

</head>

<body>
    <header class="image-bg-fluid-height">
        <a href="http://apublica.org" target="_blank" id="img-logo"><img class="img-responsive" src="/img/logo.png" alt=""></a>
        <h1 id="site-title">Especial Amazônia em Disputa </h1>

        <div class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="/img/Site1Publica.jpg" />
              </div>
              <div class="item">
                <img src="/img/Site2Publica.jpg" />
              </div>
              <div class="item">
                <img src="/img/Site3Publica.jpg" />
              </div>
              <div class="item">
                <img src="/img/Site4Publica.jpg" />
              </div>
              <div class="item">
                <img src="/img/Site5Publica.jpg" />
              </div>
              <div class="item">
                <img src="/img/Site7Publica.jpg" />
              </div>
            </div>
        </div>
    </header>
  <!-- Navigation -->
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Home</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/reportagens">Reportagens</a></li>
                    <li><a href="/base-de-dados">Base de Dados</a></li>
                    <li><a href="/videos">Vídeos</a></li>
                    <li><a href="/mapas">Mapas</a></li>
                    <li><a href="/infograficos">Infográficos</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Content Section -->
    <section>
        <div class="container">
            @yield('content')
        </div>
    </section>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                  <div id="footer-menu">
                    <a href="/o-projeto">O projeto</a> |
                    <a href="/quem-somos">Quem somos</a> |
                    <a href="/parceiro">Parceiro</a> |
                    <a href="/contato">Contato</a>
                  </div>
                  </ul>
                </div>
            </div>
            <div class="row social-media">
              <a href ="http://www.facebook.com/agenciapublica" target="_blank" class="social-icon facebook">Facebook</a>
              <a href ="https://twitter.com/agenciapublica" target="_blank" class="social-icon twitter">Twitter</a>
              <a href ="http://www.youtube.com/apublica" target="_blank" class="social-icon youtube">Youtube</a>
            </div>
        </div>
        <!-- /.container -->
    </footer>

</body>

</html>
